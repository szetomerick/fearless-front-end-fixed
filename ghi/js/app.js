window.addEventListener('DOMContentLoaded', async () => {


    const url = 'http://localhost:8000/api/conferences/';


    function createCard(name, description, pictureUrl, dateStarts, dateEnds, location) {
        return `
        <div class="card">
            <div class ="shadow p-1 mb-10 bg-body rounded">
            <img src=${pictureUrl} class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">${name}</h5>
              <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
              <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                <small class="text-muted"> ${dateStarts} - ${dateEnds} </small>
                </div>
            </div>
          </div>
        </div>
        `;
      }

    try {
      const response = await fetch(url);

      if (!response.ok) {
        alert("response not ok")
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const starts = new Date(details.conference.starts);
            const dateStarts = starts.toLocaleDateString()
            const ends = new Date(details.conference.ends);
            const dateEnds = ends.toLocaleDateString()
            const title = details.conference.name;
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(title, description, pictureUrl, dateStarts, dateEnds,location);
            const column = document.querySelector('.card-group');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
      console.error(e)
      alert("error for your catch");
    }

  });



